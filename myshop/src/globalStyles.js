import {injectGlobal, css} from 'styled-components';

export const colors = {
    GRAY : "#E4E8F0",
    RED : "#E50038",
    WHITE : "#FFFFFF",
    GREEN : "#01FFD6",
    DARK : "#183748"
}