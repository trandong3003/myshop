import React, { Component } from 'react';
import styled from 'styled-components';
import buttonStyles from './buttonStyles';

const Button = styled.button`
width: 170px;
padding-top: 30px;
padding-bottom: 30px;
text-align: center;
color: #000;
text-transform: uppercase;
font-weight: 600;
margin-left: 30px;
margin-bottom: 30px;
cursor: pointer;
display: inline-block;
    ${({ className }) => className ? '' : buttonStyles}
`;

class index extends Component {


    render() {
        return (
            <div>
                <Button className="">button</Button>
            </div>
        );
    }
}

export default index;