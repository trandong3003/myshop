import { css } from 'styled-components';
import { colors } from '../../globalStyles';

const buttonStyles = css`
    text-align: center;
    border-radius: 50px;
    padding: 26px;
    color: ${colors.WHITE}
    background: #BD3381;
    transition: all 0.2s ease-out 0s;
`;

export default buttonStyles