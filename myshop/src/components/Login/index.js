import React, { Component } from 'react';
import {ContainerLogin, Limiter, Wrap, Form, Form_Titile, Wrap_Input, Input, Span_Forcus_Input, Login_Form_Button, Wrap_Login_Form_Button, Login_Form_Bgbt, Button_Login} from './styledComponents';
class index extends Component {
    
    render() {
        return (
            <Limiter>
                <ContainerLogin>
                    <Wrap>
                        <Form>
                            <Form_Titile> Login </Form_Titile>
                        </Form>
                        <Wrap_Input>
                            <Input type = "text" value="Email"/>
                            <Span_Forcus_Input data-placeholder="Email"></Span_Forcus_Input>
                        </Wrap_Input>

                        <Wrap_Input>
                            <Input type = "password" value="Password"/>
                        </Wrap_Input>
                        <Login_Form_Button>
                            <Wrap_Login_Form_Button>
                            {/* <Login_Form_Bgbt></Login_Form_Bgbt> */}
                            <Button_Login>Login</Button_Login>
                            </Wrap_Login_Form_Button>
                        </Login_Form_Button>
                    </Wrap>
                </ContainerLogin>
            </Limiter>
        );
    }
}

export default index;