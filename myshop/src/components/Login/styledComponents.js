import styled from 'styled-components';

export const Limiter = styled.div`
    width: 100%;
    margin: 0 auto
`;

export const ContainerLogin = styled.div`
    width: 100%;
    min-height: 100vh;
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-flexbox;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    padding: 15px;
    background: #f2f2f2;
`;

export const Wrap = styled.div`
    width: 390px;
    background: #fff;
    border-radius: 10px;
    overflow: hidden;
    padding: 77px 55px 33px 55px;

    box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
    -o-box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
    -ms-box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
`;
/*[Form]*/
export const Form = styled.form`
    width: 100%;
`;

export const Form_Titile = styled.span`
    display: block;
    font-family: poppins-Bold;
    font-size: 30px;
    color: #333333;
    line-height: 1.2;
    text-align: center;
`;
/*========================*/
/* [Input] */
export const Wrap_Input = styled.div`
    width: 100%;
    position: relative;
    border-bottom: 2px solid #adadad;
    margin-bottom: 37px;
`;

export const Input = styled.input`
    outline: none;
    border: none;
    font-family: Poppins-Regular;
    font-size: 15px;
    color: #555555;
    line-height: 1.2;

    display: block;
    width: 100%;
    height: 45px;
    background: transparent;
    padding: 0 5px;
`;

/*[] */
export const Span_Forcus_Input = styled.span`
    position: absolution;
    display: block;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    pointer-events: none;

    &::before {
        content: "";
        display: block;
        position: absolute;
        bottom: -2px;
        left: 0;
        width: 0;
        height: 2px;

        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
      
        background: #6a7dfe;
        background: -webkit-linear-gradient(left, #21d4fd, #b721ff);
        background: -o-linear-gradient(left, #21d4fd, #b721ff);
        background: -moz-linear-gradient(left, #21d4fd, #b721ff);
        background: linear-gradient(left, #21d4fd, #b721ff);
    }
`;

export const Login_Form_Button = styled.div`
    display: -webkit-box;
    dispaly: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    padding-top:13px;
`;

export const Wrap_Login_Form_Button = styled.div`
    width: 100%;
    dispaly: block;
    postion: relative;
    z-index: 1;
    border-radius: 25px;
    overflow: hidden;
    margin: 0 auto;
`;

export const Login_Form_Bgbt = styled.div`
    postion: absolute;
    z-index: -1;
    width: 300%;
    height: 100%;
    background: #a64bf4;
    backgound: -webkit-linear-gradient(right, #21d4fd, #b721ff, #21d4fd, #b721ff);
    background: -o-linear-gradient(right, #21d4fd, #b721ff, #21d4fd, #b721ff);
    background: -moz-liear-gradient(right, #21d4fd, #b721ff, #21d4fd, #b721ff);
    background: linear-gradient(right, #21d4fd, #b721ff, #21d4fd, #b721ff);
    top: 0;
    left: 100%;

    -webkit-transition: all 0.4s;
    -o-transition: alll 0,4s;
    -moz-transition: all 0.4s;
    transition: all 0.4s;
`;

export const Button_Login = styled.button`
    font-family: Poppins-Medium;
    font-size: 15px;
    color: #fff;
    line-height: 1.2;
    text-tranform: uppercase;

    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    dispaly: -ms-flexbox;
    display: flex;
    jusstify-content: center;
    align-items: center;
    padding: 0 20px;
    width: 100%;
    height: 50px;
`;